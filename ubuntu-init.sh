#!/bin/bash
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) restricted"
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) multiverse"
sudo apt update && sudo apt upgrade -y
sudo apt install -y linux-tools-virtual
sudo apt install -y linux-cloud-tools-virtual
sudo apt install -y mc
echo "blacklist floppy" | sudo tee /etc/modprobe.d/blacklist-floppy.conf
echo "blacklist i2c_piix4" | sudo tee /etc/modprobe.d/blacklist-i2c_piix4.conf
echo "blacklist intel_rapl" | sudo tee /etc/modprobe.d/blacklist-intel_rapl.conf
sudo timedatectl set-timezone Europe/Kiev
sudo update-initramfs -u
sudo reboot
